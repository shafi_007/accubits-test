const jwt = require("jsonwebtoken");

function verifyToken(req, res, next) {
	const bearerToken = req.header("Authorization");

	if (typeof bearerToken !== "undefined") {
		jwt.verify(bearerToken, "secret", (err, user) => {
			if (err) {
				return res.sendStatus(401);
			} else {
				req.user = user;
				next();
			}
		});
	} else {
		res.sendStatus(401);
	}
}

module.exports = verifyToken;
