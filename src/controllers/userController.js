const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/User");

module.exports = {
	// Create new User

	createUser: async (req, res) => {
		try {
			const { name, password, email } = req.body;

			if (!email || !password || !name) {
				return res.status(400).json({ isSuccess: false, message: "Required field is missing !" });
			}
			const existingUser = await User.findOne({ email });
			if (!existingUser) {
				const hashPassword = await bcrypt.hash(password, 10);
				const user = new User({
					password: hashPassword,
					email,
					name,
				});
				await user.save();

				res.json({
					isSuccess: true,
					message: "User registered Successfully!",
				});
			} else res.status(400).json({ isSuccess: false, message: "user already exist" });
		} catch (error) {
			return res.status(422).json({ message: "internel server error" });
		}
	},

	// Login User

	loginUser: async (req, res) => {
		try {
			const { email, password } = req.body;

			if (!email || !password) {
				return res.status(400).json({ message: "Required field is missing !" });
			}

			let user = await User.findOne({ email });

			if (!user) {
				return res.status(400).json({ message: "User not found please register insted!!" });
			}

			if (user && (await bcrypt.compare(password, user.password))) {
				const userResponse = {
					_id: user._id,
					email: user.email,
					name: user.name,
				};

				return jwt.sign(userResponse, "secret", (err, token) => {
					return res.json({
						user: token,
						user_id: userResponse._id,
						email: userResponse.email,
						name: userResponse.name,
					});
				});
			} else {
				return res.status(400).json({ message: "Email or Password Does not match" });
			}
		} catch (error) {
			console.log(error);
			return res.status(422).json({ message: "Unexpected error..!!" });
		}
	},

	// Get user

	getUserById: async (req, res) => {
		const authUser = req.user;
		try {
			const user = await User.findById(authUser._id).select("-password");
			res.send({ isSuccess: true, user });
		} catch (error) {
			res.status(400).json({
				message: "user doesnot exist",
			});
		}
	},

	//Get All users

	getAllUsers: async (req, res) => {
		try {
			const users = await User.find().select("-password");
			res.send({ isSuccess: true, users });
		} catch (error) {
			return res.status(422).json({ message: "Unexpected error..!! " });
		}
	},
};
