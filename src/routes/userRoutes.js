const express = require("express");

const UserController = require("../controllers/userController");
const verifyToken = require("../middlewares/verifyToken");

const routes = express.Router();

routes.post("/sign-up", UserController.createUser);
routes.post("/sign-in", UserController.loginUser);
routes.get("/users", verifyToken, UserController.getAllUsers);
routes.get("/user", verifyToken, UserController.getUserById);

module.exports = routes;
