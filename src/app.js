const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const Redis = require('ioredis');
const session = require('express-session');
const connectRedis = require('connect-redis');

const UserRoutes = require("./routes/userRoutes");

const app = express();
const PORT = process.env.PORT || 8000;

const MONGO_STRING = `mongodb+srv://shafi_700:jFGjcgM3jOlR2tWQ@cluster0-ainaq.mongodb.net/Test?retryWrites=true&w=majority
`;

if (process.env.NODE_ENV !== `production`) {
	require("dotenv").config();
}

const RedisStore = connectRedis(session);

// const client = new Redis({
// 	port:6379,
// 	host:'localhost',
// 	password:'accubits',
// })


// app.use(session({
// 	store: new RedisStore({client}),
// 	secret:'qwerty',
// 	resave:false,
// }))

//DB connection
try {
	mongoose.connect(MONGO_STRING, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	console.log(`DB connected!`);
} catch (error) {
	console.log(error);
}

app.use(cors());
app.use(express.json());
app.use("/api/acubits", UserRoutes);

app.listen(PORT, () => {
	console.log(`App listening on port ${PORT}!`);
});
